require 'cinch'

# Who should be able to access these plugins
$admin = "alextee[m]"

bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.freenode.org"
    c.nick = "zbot"
    c.channels = ["#zrythm"]
  end

  helpers do
    def is_admin?(user)
      true if user.nick == $admin
    end
  end

  on :message, "hello" do |m|
    m.reply "Hello, #{m.user.nick}"
  end
end

bot.start
