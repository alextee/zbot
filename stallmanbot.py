#!/usr/bin/python3
#
# Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import time
import re
from matrix_client.client import MatrixClient
import urllib
from bs4 import BeautifulSoup

if len(sys.argv) != 4:
    print("USAGE: ./stallmanbot.py username password room")
    sys.exit(1)

USERNAME = sys.argv[1]
PASSWORD = sys.argv[2]
ROOMNAME = sys.argv[3]

keys_and_responses = {
    r'^(?!.*(gnu|gnoo)).*linu(x|ks).*$':
    """I'd just like to interject for moment. What you're refering to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX.
Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called Linux, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project.
There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called Linux distributions are really distributions of GNU/Linux!""",
    r'rtfm': 'https://manual.zrythm.org/en/',
    r'new issue': 'https://savannah.nongnu.org/support/?func=additem&group=zrythm',
    r'open source|floss|foss': 'Why Open Source misses the point of Free Software: https://www.gnu.org/philosophy/open-source-misses-the-point'
}


def get_url_title(string):
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    if url:
        try:
            soup = BeautifulSoup(urllib.request.urlopen(url[0]))
            return soup.title.string
        except:
            return None
    else:
        return None

# checks if the link is a youtube url, and converts to invidio.us
#if not a youtube url, returns None
def invidious_ize(string):
    key = r'(.*)(youtube.com|youtu.be)(.*)'
    if re.search(key, string.lower(), re.I):
        return re.sub(key, r'\1invidio.us\3', string)
    else:
        return None

class StallmanBot():
    def __init__(self, username, password, roomname):
        # connect to room
        self.client = MatrixClient("http://matrix.org")
        self.token = self.client.login_with_password(username=username,
                                                     password=password)
        print('logged in')
        self.room = self.client.join_room(roomname)
        print('joined room')
        self.room = self.client.join_room(roomname)

        # add bot reactions
        self.room.add_listener(self.on_message)
        self.client.start_listener_thread()

    def on_message(self, room, event):
        if event['type'] == "m.room.message":
            if USERNAME in event['sender']:
                return
            if event['content']['msgtype'] == "m.text":
                message = event['content']['body']
                invidious = invidious_ize(message)
                url_title = get_url_title(invidious) if invidious else get_url_title(message)
                if url_title:
                    self.room.send_text('URL: "' + url_title + '"')
                if invidious:
                    self.room.send_text(invidious)
                else:
                    for key in keys_and_responses:
                        if re.search(key, message.lower(), re.I):
                            for message in keys_and_responses[key].split('\n'):
                                self.room.send_text(message)


StallmanBot(USERNAME, PASSWORD, ROOMNAME)
while True:
    time.sleep(1)
